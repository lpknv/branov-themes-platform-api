package com.gitlab.branovthemesplatformbackendapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gitlab.branovthemesplatformbackendapi.model.Customer;
import com.gitlab.branovthemesplatformbackendapi.model.Theme;
import com.gitlab.branovthemesplatformbackendapi.repository.CustomerRepository;
import com.gitlab.branovthemesplatformbackendapi.repository.ThemeRepository;

@RestController
public class ThemeController {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ThemeRepository themeRepository;

    @GetMapping("/themes")
    public ResponseEntity<List<Theme>> index() {
        return new ResponseEntity<>(themeRepository.findAll(), HttpStatus.OK);
    }

    @PostMapping("/theme/add")
    public ResponseEntity<String> newTheme(@RequestBody Theme newTheme) {
      themeRepository.save(newTheme);
      return new ResponseEntity<>("Theme created!",HttpStatus.CREATED);
    }

    @PostMapping("/customer/add")
    public ResponseEntity<Customer> newCustomer(@RequestBody Customer newCustomer) {
      customerRepository.save(newCustomer);
      return new ResponseEntity<Customer>(newCustomer, HttpStatus.CREATED);
    }

    @GetMapping("/customers")
    public ResponseEntity<List<Customer>> customers() {
      customerRepository.deleteAll();
      return new ResponseEntity<List<Customer>>(customerRepository.findAll(), HttpStatus.OK);
    }
}
