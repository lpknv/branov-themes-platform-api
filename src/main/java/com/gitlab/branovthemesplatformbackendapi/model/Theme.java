package com.gitlab.branovthemesplatformbackendapi.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("themes")
public class Theme {
  @Id
  public String id;

  public String name;
  public Double price;

  public Theme() {}

  public Theme(String name, Double price) {
    super();
    this.name = name;
    this.price = price;
  }

  public String getId() {
      return id;
  }

  public void setId(String id) {
      this.id = id;
  }

  public String getName() {
      return name;
  }

  public void setName(String name) {
      this.name = name;
  }

  public Double getPrice() {
      return price;
  }

  public void setPrice(Double price) {
      this.price = price;
  }

  @Override
  public String toString() {
    return String.format(
      "Theme[id=%s, name='%s', price='%s']",
      id, name, price);
  }
}
