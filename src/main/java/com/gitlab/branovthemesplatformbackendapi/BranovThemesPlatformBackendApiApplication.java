package com.gitlab.branovthemesplatformbackendapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.gitlab.branovthemesplatformbackendapi.model.Theme;
import com.gitlab.branovthemesplatformbackendapi.repository.ThemeRepository;

@SpringBootApplication
@EnableMongoRepositories
public class BranovThemesPlatformBackendApiApplication  {

	@Autowired
  private ThemeRepository themeRepository;
	public static void main(String[] args) {
		SpringApplication.run(BranovThemesPlatformBackendApiApplication.class, args);
	}
}
