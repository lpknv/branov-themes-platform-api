package com.gitlab.branovthemesplatformbackendapi.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.gitlab.branovthemesplatformbackendapi.model.Theme;

public interface ThemeRepository extends MongoRepository<Theme, String> {
}
